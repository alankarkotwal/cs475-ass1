#version 330

in vec4 color;
in vec4 pos;
uniform int lighting;
out vec4 frag_color;

void main () 
{
    // This is not a true lighting scheme
    // To get proper lighting, we should consider the normals, too
    // However this gives a good approximation and makes edges visible, which was
    // the purpose of adding lighting.
    if(lighting != 0) {
        frag_color = color*min(1,0.5*lighting/length(vec4(0,0,2,1) - pos));
    } else {
        frag_color = color;
    }
}
