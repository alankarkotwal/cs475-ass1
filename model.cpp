//  Modeller Class for CS475 Assignment-1
//  model.cpp
//  
//
//  Created by Alankar Kotwal on 09/08/15.
//  <alankar.kotwal@iitb.ac.in>
//  12D070010

#include <model.hpp>


/*
    Default constructor for the class
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Sets up the OpenGL context for this instance of the class. Creates the first model and
    binds it to the context. Except for the callbacks, which are expected to be static.
    This causes problems, so the callbacks are in main().
*/
modeler::modeler() {
    
    if(!glfwInit())
        exit(0);
    
    glfwSetErrorCallback(csX75::error_callback);
    
    //We want OpenGL 4.0
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    //This is for MacOSX - can be omitted otherwise
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    //We don't want the old OpenGL
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    // Create a windowed mode window and its OpenGL context
    this->window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "Modeler", NULL, NULL);
    if (!this->window) {
        glfwTerminate();
        exit(0);
    }
    
    //! Make the window's context current
    glfwMakeContextCurrent(this->window);
    
    //Initialize GLEW
    //Turn this on to get Shader based OpenGL
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        //Problem: glewInit failed, something is seriously wrong.
        std::cerr << BOLDRED << "[ERR ] GLEW Init Failed : %s." << RESET << std::endl;
    }

    //Framebuffer resize callback
    glfwSetFramebufferSizeCallback(window, csX75::framebuffer_size_callback);
    
    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(this->window, GLFW_STICKY_KEYS, GL_TRUE);
    
    //Initialize GL state
    csX75::initGL();
    initShadersGL();
    
    this->uRot = glGetUniformLocation(shaderProgram, "uRotationMatrix");
    this->uLighting =  glGetUniformLocation(shaderProgram, "lighting");
    this->vPos = glGetAttribLocation(shaderProgram, "vPosition");
    this->vCol = glGetAttribLocation(shaderProgram, "vColor");
    
    this->xrot = 0.0;
    this->yrot = 0.0;
    this->zrot = 0.0;
    this->xpos = 0.0;
    this->ypos = 0.0;
    this->zpos = 0.0;
    
    model m0;
    //Ask GL for a Vertex Attribute Object (vao)
    glGenVertexArrays(1, &m0.vao);
    //Set it as the current array to be used by binding it
    glBindVertexArray(m0.vao);
    
    //Ask GL for a Vertex Buffer Object (vbo)
    glGenBuffers(1, &m0.vbo);
    //Set it as the current buffer to be used by binding it
    glBindBuffer(GL_ARRAY_BUFFER, m0.vbo);
    
    this->models.push_back(m0);

    centroid = glm::vec3(0.0f, 0.0f, 0.0f);
    lighting = 3;
    modelingColor = glm::vec4(1,0,0,1);

    this->inMenu = false;
    this->activeModel = 0;
    this->scale = 1.0;
}


/*
    Default destructor
    ~~~~~~~~~~~~~~~~~~
    Kill GLFW.
*/
modeler::~modeler() {
    
    glfwTerminate();
}


/*
    Initialize shaders
    ~~~~~~~~~~~~~~~~~~
    Like the name says. I didn't write this.
*/
void modeler::initShadersGL() {

    std::string vertex_shader_file("vs.glsl");
    std::string fragment_shader_file("fs.glsl");
    
    std::vector<GLuint> shaderList;
    shaderList.push_back(csX75::LoadShaderGL(GL_VERTEX_SHADER, vertex_shader_file));
    shaderList.push_back(csX75::LoadShaderGL(GL_FRAGMENT_SHADER, fragment_shader_file));
    
    this->shaderProgram = csX75::CreateProgramGL(shaderList);
}


/*
    getWindow
    ~~~~~~~~~
    Return window handle for the callbacks
*/
GLFWwindow* modeler::getWindow() {
    
    return window;
}


/*
    setMode
    ~~~~~~~
    Enable setting the modeler mode for the callbacks
*/
void modeler::setMode(model_mode_t _mode) {
    
    mode = _mode;
    
    if(mode == MODE_INSPECT) {
        finishPolygon();
        updateCentroid();
        std::cout << BOLDGREEN << "[INFO] Inspection mode activated." << RESET << std::endl;
    } else {
        std::cout << BOLDGREEN << "[INFO] Modeling mode activated." << RESET <<std::endl;
        modelingPoints = std::vector<glm::vec4>();
    }
}


/*
    getMode
    ~~~~~~~
    Get the modeler mode
*/
model_mode_t modeler::getMode() {
    
    return mode;
}


/*
    updateCallback
    ~~~~~~~~~~~~~~
    The rendering function that runs again and again. Scan all models in the modeler, initialise all VBOs and VAOs,
    perform all necessary transformations, and draw to the screen.
*/
void modeler::updateCallback() {
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shaderProgram);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);
    
    //for (int i=0; i<this->models.size(); i++) {
        
    // Copy all model data to respective VBOs.
    glBindVertexArray(this->models[activeModel].vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->models[activeModel].vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*(this->models[activeModel].points.size()+this->models[activeModel].colors.size()), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(glm::vec4)*this->models[activeModel].points.size(), &this->models[activeModel].points[0]);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*this->models[activeModel].points.size(), sizeof(glm::vec4)*this->models[activeModel].colors.size(), &this->models[activeModel].colors[0]);
        
    glEnableVertexAttribArray(this->vPos);
    glVertexAttribPointer(this->vPos, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    
    glEnableVertexAttribArray(this->vCol);
    glVertexAttribPointer(this->vCol, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(glm::vec4)*this->models[activeModel].points.size()));
    

    
    glm::mat4 rot = glm::mat4(1.0f);

    rot = glm::translate(rot, glm::vec3(this->xpos, this->ypos, this->zpos));
    rot = glm::rotate(rot, this->xrot, glm::vec3(1.0f, 0.0f, 0.0f));
    rot = glm::rotate(rot, this->yrot, glm::vec3(0.0f, 1.0f, 0.0f));
    rot = glm::rotate(rot, this->zrot, glm::vec3(0.0f, 0.0f, 1.0f));
    rot = glm::scale(rot, glm::vec3(this->scale, this->scale, this->scale));
    rot = glm::translate(rot, glm::vec3(-centroid.x, -centroid.y, - centroid.z));

    if (this->models[this->activeModel].points.size() > 0) {
        this->oldRot = rot;
    } else {
        this->oldRot = glm::mat4(1.0f);
    }
    
   // std::cout<<xrot<<yrot<<zrot<<":"<<scale<<":"<<centroid.x<<centroid.y<<centroid.z<<":"<<xpos<<ypos<<zpos<<std::endl;
    
    glUniformMatrix4fv(this->uRot, 1, GL_FALSE, glm::value_ptr(rot));
    glUniform1i(this->uLighting, this->lighting);

    glDrawArrays(GL_TRIANGLES, 0, this->models[activeModel].points.size());

    glDisableVertexAttribArray(0);
    glfwSwapBuffers(this->window);
}

/*
    updateCentroid
    ~~~~~~~~~~
    Recalculate the centroid
*/
void modeler::updateCentroid() {
    centroid = glm::vec3(0.0f, 0.0f, 0.0f);
    
    if(this->models[this->activeModel].points.size() >= 0) {
        
        float nPoints = 0;
        for(int i=0; i<(this->models[this->activeModel].points.size()); i++) {
        
            centroid.x = centroid.x + this->models[this->activeModel].points[i].x;
            centroid.y = centroid.y + this->models[this->activeModel].points[i].y;
            centroid.z = centroid.z + this->models[this->activeModel].points[i].z;
            nPoints++;
        }
        
        centroid.x = centroid.x / nPoints;
        centroid.y = centroid.y / nPoints;
        centroid.z = centroid.z / nPoints;
    }
    else {
        centroid.x = 0;
        centroid.y = 0;
        centroid.z = 0;
    }

}

/*
    flushTransform()
    ~~~~~~~~~~
    Apply all rotation transforms to model (to store a different view)
*/
void modeler::flushTransform() {
    finishPolygon();   
    for(int i=0; i<models[activeModel].points.size(); i++) {
        models[activeModel].points[i] = oldRot*models[activeModel].points[i];
    }
    xpos=ypos=zpos=0;
    xrot=yrot=zrot=0;
    scale=1.0;
    oldRot = glm::mat4(1.0f);
    updateCentroid();
}


/*
    finishPolygon
    ~~~~~~~~~~
    Finish drawing a polygon so that we can start a new one
    Effectively flushes modeling buffers to main
*/
void modeler::finishPolygon() {
    // If <2, we haven't yet drawn anything, discard
    if (modelingPoints.size() > 2) {
        modelingPoints = std::vector<glm::vec4>();

        // While it is possible to update the centroid here, the model jumps, and we don't want
        // to do that when drawing multiple polygons
        // updateCentroid();
    }
}

/*
    storeModel
    ~~~~~~~~~~
    Save the current model (all points on the screen) to a model file.
*/
void modeler::storeModel() {
    
    this->inMenu = true;
    
    std::cout << BOLDYELLOW << "[QUES] Enter filename: " << RESET;
    std::string filename;
    std::cin >> filename;
    
    std::ofstream outfile("models/"+filename+".raw", std::ofstream::out);
    
    if(outfile.is_open()) {
        
        outfile << "# File: " + filename << std::endl;

        for (int j=0; j<this->models.size(); j++) {
            
            for (int i=0; i<this->models[j].points.size(); i++) {
                
                outfile <<  this->models[j].points[i].x << " " <<
                            this->models[j].points[i].y << " " <<
                            this->models[j].points[i].z << " " <<
                            this->models[j].colors[i].r << " " <<
                            this->models[j].colors[i].g << " " <<
                            this->models[j].colors[i].b << std::endl;
            }
        }
    
        outfile.close();
        std::cout << BOLDGREEN << "[INFO] File models/"+filename+".raw saved!" << RESET << std::endl;
    }
    else {
        
        std::cerr << BOLDRED << "[ERR ] File models/"+filename+".raw cannot be saved!" << RESET << std::endl;
    }
    
    this->inMenu = false;
}


/*
    loadModel
    ~~~~~~~~~
    Load model from a file
*/
void modeler::loadModel() {
    
    // If we were modeling, flush the polygon and clear the modeling buffers
    finishPolygon();
    this->inMenu = true;
    
    std::cout << BOLDYELLOW << "[QUES] Enter filename: " << RESET;
    std::string filename;
    std::cin >> filename;
    
    std::ifstream infile("models/"+filename+".raw", std::ifstream::in);
    
    if(infile.is_open()) {
        
        std::vector<glm::vec4> posVec;
        std::vector<glm::vec4> colVec;
        
        bool err = 0;
        std::string line;
        
        while(1) {
            
            if(std::getline(infile, line)) {
            
                if(line.length()!=0 && line[0] != '#') {
                    
                    std::istringstream iss(line);
                    
                    float temp[6];
                    int i=0;
                    
                    while(!iss.eof()) {
                        iss >> temp[i];
                        i++;
                    }
                    
                    if(i == 6) {
                        posVec.push_back(glm::vec4(temp[0], temp[1], temp[2], 1.0));
                        colVec.push_back(glm::vec4(temp[3], temp[4], temp[5], 1.0));
                    }
                    else {
                        err = 1;
                    }
                }
            }
            else {
                
                break;
            }
        }
        
        if(!err) {
            
            model m;
            glGenVertexArrays(1, &m.vao);
            glGenBuffers (1, &m.vbo);
            m.points = posVec;
            m.colors = colVec;
            this->models.push_back(m);
            this->activeModel = models.size()-1;
            updateCentroid();
            std::cout << BOLDGREEN << "[INFO] File models/"+filename+".raw loaded!" << RESET << std::endl;
        }
        
        infile.close();
    }
    else {
        
        std::cerr << BOLDRED << "[ERR ] File models/"+filename+".raw cannot be opened!" << RESET << std::endl;
    }
    
    this->inMenu = false;
}

/*
    changeActiveModel
    ~~~~~~~~~~~~~~~~~
    Change the model that is displayed and edited in the modeler
*/
void modeler::changeActiveModel() {

    // If we were modeling, flush the polygon and clear the modeling buffers
    finishPolygon();

    int modelNo;
    std::cout << BOLDGREEN << "[INFO] There are " << models.size() << " loaded models." << RESET << std::endl << BOLDYELLOW "[QUES] Enter the model number you want to make active: " << RESET;
    std::cin >> modelNo;
    modelNo = modelNo;
    
    if(modelNo > models.size()) {
        
        std::cerr << BOLDRED << "[ERR ] That exceeds the number of loaded models." << RESET << std::endl;
    }
    else if(modelNo < 1) {
        
        std::cerr << BOLDRED << "[ERR ] Invalid option." << RESET << std::endl;
    }
    else {
        
        this->activeModel = modelNo-1;
        updateCentroid();
        std::cout << BOLDGREEN << "[INFO] Model number " << modelNo << " made active!" << RESET << std::endl;
    }
}

/*
    changeModelingColor
    ~~~~~~~~~~~~~~~~~
    Change the color used for modeling
*/
void modeler::changeModelingColor() {

    int modelNo;
    std::cout << BOLDYELLOW "[QUES] Enter the r,g,b,a (space-separated, in range 0-1) values of the color you wish to use " << RESET;
    std::cin>>modelingColor.r>>modelingColor.g>>modelingColor.b>>modelingColor.a;
}

/*
    addPoint
    ~~~~~~~~
    Add a point to the clicked location at the selected z-coordinate
*/
void modeler::addPoint(float xCur, float yCur) {
    glm::vec4 viewingPos = glm::vec4(xCur, yCur, modelingZ/Z_SCALING, 1);
    modelingPoints.push_back(glm::inverse(oldRot)*viewingPos);
    int len = modelingPoints.size();
    if (len > 2) {
        models[activeModel].points.push_back(modelingPoints[0]);
        models[activeModel].points.push_back(modelingPoints[len-2]);
        models[activeModel].points.push_back(modelingPoints[len-1]);
        models[activeModel].colors.push_back(modelingColor);
        models[activeModel].colors.push_back(modelingColor);
        models[activeModel].colors.push_back(modelingColor);
    }
}

/*
    rmPoint
    ~~~~~~~~
    Removes last added point
*/
void modeler::rmPoint() {
    if(modelingPoints.size() >  2) {
        modelingPoints.pop_back();
        models[activeModel].points.pop_back();
        models[activeModel].points.pop_back();
        models[activeModel].points.pop_back();
        models[activeModel].colors.pop_back();
        models[activeModel].colors.pop_back();
        models[activeModel].colors.pop_back();
    }
}

/*
    mainLoop
    ~~~~~~~~
    Loop wrapper. Run updateCallback again and again.
*/
void modeler::mainLoop() {
    
    while (glfwWindowShouldClose(this->window) == 0) {
        
        updateCallback();
        glfwPollEvents();
    }
    
    std::cout << BOLDGREEN << "[INFO] Bye." << RESET << std::endl;
    glfwTerminate();
}

// END model.cpp