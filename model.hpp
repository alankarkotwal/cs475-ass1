//  Modeller Class for CS475 Assignment-1
//  model.hpp
//  
//
//  Created by Alankar Kotwal on 09/08/15.
//  <alankar.kotwal@iitb.ac.in>
//  12D070010

#ifndef ____model__
#define ____model__

#include <iostream>
#include <vector>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <gl_framework.hpp>
#include <shader_util.hpp>
#include <color.hpp>

#define BUFFER_OFFSET(offset)   ((GLvoid*)(offset))

#define MODEL_ANGLE_STEP 0.1
#define MODEL_POS_STEP 0.02
#define MODEL_SCALE_STEP 0.1
#define NUM_STEPS 5
#define WIN_WIDTH 512
#define WIN_HEIGHT 512
#define Z_SCALING 10.0

typedef enum {
    
    MODE_MODEL,
    MODE_INSPECT
    
} model_mode_t;

typedef struct {
    
    GLuint vao;
    GLuint vbo;
    std::vector<glm::vec4> points;
    std::vector<glm::vec4> colors;
    
} model;

class modeler {
    
    private:
    GLFWwindow* window;
    model_mode_t mode;
    GLuint shaderProgram;
    std::vector<model> models;
    int activeModel;
    float modelZ;
    
    GLuint vPos, vCol, uRot, uLighting;
    
    public:
    bool inMenu;
    int lighting;
    GLfloat xpos, ypos, zpos;
    GLfloat xrot, yrot, zrot;
    GLfloat scale;
    modeler();
    ~modeler();
    void initShadersGL();
    void updateCallback();
    void keyCallback(int key, int scancode, int action, int mods);
    void mouseCallback(int button, int action, int mods);
    GLFWwindow* getWindow();
    void setMode(model_mode_t);
    model_mode_t getMode();
    void storeModel();
    void loadModel();
    void changeActiveModel();
    void changeModelingColor();
    void addPoint(float xCur, float yCur);
    void rmPoint();
    void mainLoop();
    void updateCentroid();
    void finishPolygon();
    void flushTransform();

    glm::vec3 centroid;

    std::vector<glm::vec4> modelingPoints;
    glm::vec4 modelingColor;
    int modelingZ;
    glm::mat4 oldRot;
};

#endif /* defined(____model__) */
