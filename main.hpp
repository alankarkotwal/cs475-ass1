//  Modeller Class for CS475 Assignment-1
//  main.hpp
//  
//
//  Created by Alankar Kotwal on 06/08/15.
//  <alankar.kotwal@iitb.ac.in>
//  12D070010

#include <iostream>
#include <string>
#include <model.hpp>

modeler m;

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
        
    if (action == GLFW_PRESS) {
            
        if (!m.inMenu) {
            
            switch(key) {
                case GLFW_KEY_ESCAPE: {
                    glfwSetWindowShouldClose(m.getWindow(), GL_TRUE);
                    break;
                }
                case GLFW_KEY_M: {
                    m.setMode(MODE_MODEL);
                    break;
                }
                case GLFW_KEY_I: {
                    m.setMode(MODE_INSPECT);
                    break;
                }
                case GLFW_KEY_K: {
                    m.storeModel();
                    break;
                }
                case GLFW_KEY_L: {
                    m.loadModel();
                    break;
                }
                case GLFW_KEY_TAB: {
                    m.changeActiveModel();
                    break;
                }
                case GLFW_KEY_C: {
                    m.changeModelingColor();
                    break;
                }
                case GLFW_KEY_V: {
                    if (m.lighting != 0) {
                        m.lighting = 0;
                    } else {
                        m.lighting = 1;
                    }
                    m.updateCallback();
                    break;
                }
                case GLFW_KEY_EQUAL: {
                    if (mods & GLFW_MOD_SHIFT) {
                        // + is = and SHIFT
                        m.lighting++;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_MINUS: {
                    if (m.lighting >= 1) {
                        m.lighting--;
                    }
                    m.updateCallback();
                    break;
                }
                case GLFW_KEY_R: {
                    m.xpos = 0;
                    m.ypos = 0;
                    m.zpos = 0;
                    break;
                }
                case GLFW_KEY_F: {
                    m.finishPolygon();
                    break;
                }
                case GLFW_KEY_T: {
                    m.flushTransform();
                    break;
                }
                case GLFW_KEY_LEFT: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.yrot -= MODEL_ANGLE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_RIGHT: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.yrot += MODEL_ANGLE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_UP: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.xrot -= MODEL_ANGLE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_DOWN: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.xrot += MODEL_ANGLE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_PAGE_UP: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.zrot -= MODEL_ANGLE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_PAGE_DOWN: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.zrot += MODEL_ANGLE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_O: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.zrot -= MODEL_ANGLE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_P: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.zrot += MODEL_ANGLE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_W: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.ypos += MODEL_POS_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_S: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.ypos -= MODEL_POS_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_A: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.xpos -= MODEL_POS_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_D: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.xpos += MODEL_POS_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_Z: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.zpos -= MODEL_POS_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_X: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.zpos += MODEL_POS_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_Q: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.scale -= MODEL_SCALE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                case GLFW_KEY_E: {
                    for (int i=0; i<NUM_STEPS; i++) {
                        m.scale += MODEL_SCALE_STEP/NUM_STEPS;
                        m.updateCallback();
                    }
                    break;
                }
                default: {
                    //std::cerr << "[ERR ] Command not defined" << std::endl;
                    break;
                }
            }
        }
    }
}

void mouseCallback(GLFWwindow* window, int button, int action, int mods) {
    
    if(action == GLFW_PRESS) {
        if(m.getMode() == MODE_MODEL) {
            if (mods & GLFW_MOD_SHIFT) {
                m.rmPoint();
            } else {
                double xpos, ypos;
                int w,h;
                glfwGetCursorPos(m.getWindow(), &xpos, &ypos);
                glfwGetWindowSize(m.getWindow(), &w, &h);
                //std::cout << xpos << "\t" << ypos << std::endl;
                m.addPoint(2*(xpos/w) - 1, 1- 2*(ypos/h));
            }
        }
    }
}

void scrollCallback(GLFWwindow* window, double x, double y) {
    if (y > 0) {
        m.modelingZ += 1;
        std::cout << BOLDGREEN << "[INFO] Modeling z-coordinate now " << m.modelingZ/Z_SCALING << RESET << std::endl;
    } else if (y < 0) {
        m.modelingZ -= 1;
        std::cout << BOLDGREEN << "[INFO] Modeling z-coordinate now " << m.modelingZ/Z_SCALING << RESET << std::endl;
    }
}